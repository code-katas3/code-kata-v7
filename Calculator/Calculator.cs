﻿using System;
using System.Collections.Generic;

namespace Calculators
{
    public class Calculator
    {
        string newLineDelimiter = "\n";
        string forwardSlashDelimiter = "//";
        string commaDelimiter = ",";

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            string[] delimiters = GetDelimiters(numbers);
            int[] numbersArray = GetNumbers(numbers, delimiters);
            var numberList = new List<int>();

            foreach (var number in numbersArray)
            {
                numberList.Add(number);
            }
            GetNegativeNumberCheck(numberList);

            return GetSumNumbers(numberList);
        }

        private string[] GetDelimiters(string numbers)
        {
            if (!numbers.StartsWith(forwardSlashDelimiter))
            {
                return new string[] { commaDelimiter, newLineDelimiter };
            }
            int delimiterLength = numbers.IndexOf(newLineDelimiter) - 2;
            var delimiterData = numbers.Substring(2, delimiterLength);
            if (delimiterData.Contains("["))
            {
                delimiterData = delimiterData.Substring(1, delimiterData.Length - 2);
                return delimiterData.Split(new string[] { "][" }, StringSplitOptions.RemoveEmptyEntries);
            }

            return new string[] { delimiterData };
        }

        private void GetNegativeNumberCheck(List<int> numberList)
        {
            var negativeNumbers = new List<int>();
            foreach (int number in numberList)
            {
                if (number < 0)
                {
                    negativeNumbers.Add(number);
                }
            }
            if (negativeNumbers.Count > 0)
            {
                throw new Exception("Negatives not allowed : " + string.Join(commaDelimiter, negativeNumbers));
            }
        }

        private int[] GetNumbers(string numbers, string[] delimiters)
        {
            var stringNumbers = numbers;
            if (numbers.StartsWith(forwardSlashDelimiter))
            {
                int startDelimiter = numbers.IndexOf(newLineDelimiter) + 1;
                stringNumbers = numbers.Substring(startDelimiter);
            }

            return Array.ConvertAll(stringNumbers.Split(delimiters, StringSplitOptions.None), int.Parse);
        }

        public int GetSumNumbers(List<int> cleanList)
        {
            int sum = 0;
            foreach(var number in cleanList)
            {
                if (number < 1001)
                {
                    sum += number;
                }
            }

            return sum;
        }
    }
}
